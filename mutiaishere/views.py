from django.shortcuts import render

# Create your views here.

def home(request):
	return render(request, 'home.html')
	
def experience(request):
	return render(request, 'Experience.html')
	
def registration(request):
	return render(request, 'pendaftaran.html')
	
def contactMe(request):
	return render(request, 'ContactMe.html')